window.onload = function () {
    const xhr = new XMLHttpRequest();
    const date = document.querySelector(".name");
    const btn = document.getElementById("button");
    const content = document.getElementById("one");
    xhr.open( "GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
    btn.addEventListener("click", () => {
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var data = JSON.parse(xhr.responseText);
                date.innerHTML += data[0].exchangedate;
                data.forEach((i) => {
                    if (i.rate > 25) {
                        let tr = document.createElement("tr");
                        let td1 = document.createElement("td");
                        let td2 = document.createElement("td");
                        let td3 = document.createElement("td");
                        content.appendChild(tr);
                        td1.innerHTML = i.txt;
                        td2.innerHTML = i.cc;
                        td3.innerHTML = i.rate.toFixed(2) + " грн";
                        tr.appendChild(td1);
                        tr.appendChild(td2);
                        tr.appendChild(td3);
                    }
                });
                btn.remove();
                date.setAttribute("id", "date");
            } else if (xhr.status == 400) {
                document.getElementById("output").innerHTML = "400";
            } else if (xhr.status == 500) {
                document.getElementById("output").innerHTML = "500";
            }
        };
        xhr.send();
    });
};

